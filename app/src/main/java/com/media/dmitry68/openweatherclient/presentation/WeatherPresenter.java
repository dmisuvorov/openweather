package com.media.dmitry68.openweatherclient.presentation;

import com.media.dmitry68.openweatherclient.base.BasePresenter;
import com.media.dmitry68.openweatherclient.domain.City;
import com.media.dmitry68.openweatherclient.ui.WeatherView;

public interface WeatherPresenter extends BasePresenter<WeatherView> {
    void getCityWeatherDetails(City city, boolean isConnected);
}

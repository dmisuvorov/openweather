package com.media.dmitry68.openweatherclient.framework;

import com.media.dmitry68.openweatherclient.data.CityDataSource;
import com.media.dmitry68.openweatherclient.data.client.PhotoClient;
import com.media.dmitry68.openweatherclient.data.client.WeatherClient;
import com.media.dmitry68.openweatherclient.domain.City;

import java.util.Random;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Single;

public class CityDataSourceImpl implements CityDataSource {
    private static City.Builder cityBuilder = City.newBuilder();

    @Inject
    WeatherClient weatherClient;

    @Inject
    PhotoClient photoClient;

    @Inject
    @Named("weather_api_key")
    String weatherApiKey;

    @Inject
    @Named("photo_api_key")
    String photoApiKey;

    @Inject
    Random random;

    @Inject
    CityDataSourceImpl() {

    }

    @Override
    public City getCityByName(String name) {
        return cityBuilder
                .setCityString(name)
                .build();
    }

    @Override
    public Single<City> getCityDetailsByName(City city) {
        return getCityDetailsByName(city.getCityString());
    }

    @Override
    public Single<City> getCityDetailsByName(String name) {
        return Single
                .zip(getWeatherByName(name), getPhotoUrlByName(name),
                        (temperature, photoURL) -> cityBuilder
                                .setCityString(name)
                                .setTemperature(temperature)
                                .setCityPhotoUrl(photoURL)
                                .build());
    }

    @Override
    public Single<Double> getWeatherByName(String name) {
        return weatherClient.getWeatherByCity(name, weatherApiKey)
                .flatMap(weatherResponse ->
                        Single.fromCallable(() -> weatherResponse.getMain().getTemp())
                                .onErrorReturnItem(0.0));
    }

    @Override
    public Single<String> getPhotoUrlByName(String name) {
        return photoClient.getPhotoByCity(photoApiKey, name)
                .flatMap(photoResponse ->
                        Single.fromCallable(() ->
                                photoResponse.getHits().get(random.nextInt(photoResponse.getHits().size())).getLargeImageURL())
                                .onErrorReturnItem(""));
    }
}

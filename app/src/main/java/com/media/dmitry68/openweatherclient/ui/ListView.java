package com.media.dmitry68.openweatherclient.ui;

import com.media.dmitry68.openweatherclient.domain.City;

import java.util.Set;

public interface ListView {
    void setList(Set<City> cities);

    void updateList(Set<City> cities);
}

package com.media.dmitry68.openweatherclient.base;

public interface BasePresenter<T> {
    void bind(T view);

    void unbind();
}

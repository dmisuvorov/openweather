package com.media.dmitry68.openweatherclient.di.modules;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.media.dmitry68.openweatherclient.data.client.PhotoClient;
import com.media.dmitry68.openweatherclient.data.client.WeatherClient;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {
    @Provides
    @Singleton
    public WeatherClient provideWeatherApiService(@Named("weather") Retrofit retrofit) {
        return retrofit.create(WeatherClient.class);
    }

    @Provides
    @Singleton
    @Named("weather")
    public Retrofit provideWeatherRetrofit(@Named("weather") HttpUrl baseUrl, Converter.Factory converterFactory, CallAdapter.Factory callAdapterFactory, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(callAdapterFactory)
                .client(okHttpClient)
                .build();
    }


    @Provides
    @Singleton
    public PhotoClient providePhotoApiService(@Named("photo") Retrofit retrofit) {
        return retrofit.create(PhotoClient.class);
    }

    @Provides
    @Singleton
    @Named("photo")
    public Retrofit providePhotoRetrofit(@Named("photo") HttpUrl baseUrl, Converter.Factory converterFactory, CallAdapter.Factory callAdapterFactory, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(callAdapterFactory)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    public Converter.Factory provideGsonConverterFactory(Gson gson) {
        return GsonConverterFactory.create();
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    public CallAdapter.Factory provideRxJavaCallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }
}

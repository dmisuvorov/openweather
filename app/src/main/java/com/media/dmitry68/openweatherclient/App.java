package com.media.dmitry68.openweatherclient;

import android.app.Application;
import android.content.Context;

import com.media.dmitry68.openweatherclient.di.ApplicationComponent;
import com.media.dmitry68.openweatherclient.di.DaggerApplicationComponent;
import com.media.dmitry68.openweatherclient.di.ListSubComponent;
import com.media.dmitry68.openweatherclient.di.WeatherSubComponent;
import com.media.dmitry68.openweatherclient.di.modules.AndroidModule;
import com.media.dmitry68.openweatherclient.di.modules.ListModule;
import com.media.dmitry68.openweatherclient.di.modules.WeatherModule;
import com.media.dmitry68.openweatherclient.ui.MainActivity;

public class App extends Application {
    private static ApplicationComponent component;
    private static ListSubComponent listSubComponent;
    private static WeatherSubComponent weatherSubComponent;

    public static ApplicationComponent getComponent() {
        return component;
    }


    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    public static ListSubComponent getListSubComponent(MainActivity activity) {
        if (listSubComponent == null) {
            createListSubComponent(activity);
        }
        return listSubComponent;
    }

    private static void createListSubComponent(MainActivity activity) {
        listSubComponent = component.plus(new ListModule(activity));
    }

    public static WeatherSubComponent getWeatherSubComponent() {
        if (weatherSubComponent == null) {
            createWeatherSubComponent();
        }
        return weatherSubComponent;
    }

    private static void createWeatherSubComponent() {
        weatherSubComponent = component.plus(new WeatherModule());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = createComponent();
    }

    private ApplicationComponent createComponent() {
        return DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .build();
    }

    public static void releaseListSubComponent() {
        listSubComponent = null;
    }

    public static void releaseWeatherSubComponent() {
        weatherSubComponent = null;
    }
}


package com.media.dmitry68.openweatherclient.ui;

public interface WeatherView {
    void showQueryError(String message);

    void showPhoto(String photoUrl);

    void showTemperature(String temperature);

    void showProgress();

    void dismissProgress();

    void showOfflineMessage();
}

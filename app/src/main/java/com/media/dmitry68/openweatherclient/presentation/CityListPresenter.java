package com.media.dmitry68.openweatherclient.presentation;

import com.media.dmitry68.openweatherclient.base.BasePresenter;
import com.media.dmitry68.openweatherclient.ui.ListView;


public interface CityListPresenter extends BasePresenter<ListView> {
    void getCities();

    boolean isCityValid(String city);

    void addCity(String city);
}

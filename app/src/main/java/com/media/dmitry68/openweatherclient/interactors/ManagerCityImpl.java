package com.media.dmitry68.openweatherclient.interactors;

import com.media.dmitry68.openweatherclient.data.CityRepository;
import com.media.dmitry68.openweatherclient.domain.City;

import java.util.Set;

import javax.inject.Inject;

import io.reactivex.Single;

public class ManagerCityImpl implements ManagerCity {
    @Inject
    CityRepository cityRepository;

    @Inject
    public ManagerCityImpl() {

    }

    @Override
    public Set<City> getCities() {
        return cityRepository.getCities();
    }

    @Override
    public boolean addCityToStorage(String cityName) {
        return cityRepository.addCityToStorage(cityName);
    }

    @Override
    public boolean addCityToStorage(City city) {
        return cityRepository.addCityToStorage(city);
    }

    @Override
    public Single<City> getDetailsCity(City city, boolean isConnected) {
        return cityRepository.getDetailsCity(city, isConnected);
    }
}

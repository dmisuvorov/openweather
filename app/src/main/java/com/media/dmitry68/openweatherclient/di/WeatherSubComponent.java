package com.media.dmitry68.openweatherclient.di;

import com.media.dmitry68.openweatherclient.di.modules.WeatherModule;
import com.media.dmitry68.openweatherclient.ui.WeatherActivity;

import dagger.Subcomponent;

@WeatherScope
@Subcomponent(modules = WeatherModule.class)
public interface WeatherSubComponent {
    void inject(WeatherActivity activity);
}

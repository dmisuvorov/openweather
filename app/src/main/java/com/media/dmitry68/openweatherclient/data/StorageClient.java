package com.media.dmitry68.openweatherclient.data;

import com.media.dmitry68.openweatherclient.domain.City;

import java.util.Set;

public interface StorageClient {
    boolean putCityToStorage(City city);

    City getCityFromStorage(City city);

    Set<City> getCitiesFromStorage();

    void putCitiesToStorage(Set<City> cities);
}

package com.media.dmitry68.openweatherclient.data.client;

import com.media.dmitry68.openweatherclient.domain.WeatherResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherClient {

    @GET("weather?units=metric")
    Single<WeatherResponse> getWeatherByCity(@Query("q") String cityString, @Query("appid") String appID);

}

package com.media.dmitry68.openweatherclient.utils;

import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.media.dmitry68.openweatherclient.R;

public class ImageUtil {

    public static void loadAvatarFromUrl(Context context, String url, ImageView imageView) {
        if (context == null || imageView == null) {
            return;
        }
        if (url != null) {
            Glide.with(context)
                    .load(Uri.parse(url))
                    .placeholder(R.drawable.ic_location_city_black_24dp)
                    .error(R.drawable.ic_location_city_black_24dp)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .crossFade()
                    .into(imageView);
        }
    }
}

package com.media.dmitry68.openweatherclient.presentation;

import com.media.dmitry68.openweatherclient.domain.City;
import com.media.dmitry68.openweatherclient.interactors.ManagerCity;
import com.media.dmitry68.openweatherclient.ui.WeatherView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class WeatherPresenterImpl implements WeatherPresenter {
    private WeatherView weatherView;
    private Disposable disposable;

    @Inject
    ManagerCity managerCity;

    @Inject
    WeatherPresenterImpl() {

    }

    @Override
    public void bind(WeatherView view) {
        weatherView = view;
    }

    @Override
    public void unbind() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        weatherView = null;
    }

    @Override
    public void getCityWeatherDetails(City city, boolean isConnected) {
        if (weatherView != null) {
            weatherView.showProgress();
            weatherView.showPhoto(city.getCityPhotoUrl());
        }

        disposable = managerCity.getDetailsCity(city, isConnected)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cityResponse -> {
                    if (weatherView != null) {
                        weatherView.dismissProgress();
                        if (isConnected) {
                            managerCity.addCityToStorage(cityResponse);
                        } else {
                            weatherView.showOfflineMessage();
                        }
                        weatherView.showTemperature(cityResponse.getTemperature() + " \u2103");
                        weatherView.showPhoto(cityResponse.getCityPhotoUrl());
                    }
                }, throwable -> {
                    if (weatherView != null) {
                        weatherView.dismissProgress();
                        weatherView.showQueryError(throwable.getMessage());
                    }
                });

    }
}

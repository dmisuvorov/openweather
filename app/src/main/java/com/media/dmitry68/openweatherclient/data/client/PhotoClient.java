package com.media.dmitry68.openweatherclient.data.client;

import com.media.dmitry68.openweatherclient.domain.PhotoResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PhotoClient {

    @GET("?image_type=photo&pretty=true")
    Single<PhotoResponse> getPhotoByCity(@Query("key") String apiKey, @Query(value = "q", encoded = true) String cityString);
}

package com.media.dmitry68.openweatherclient.di.modules;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import com.media.dmitry68.openweatherclient.di.ListScope;
import com.media.dmitry68.openweatherclient.presentation.CityListPresenter;
import com.media.dmitry68.openweatherclient.presentation.CityListPresenterImpl;
import com.media.dmitry68.openweatherclient.ui.MainActivity;
import com.media.dmitry68.openweatherclient.ui.adapter.CityListAdapter;

import dagger.Module;
import dagger.Provides;

@Module
public class ListModule {
    private MainActivity mainActivity;

    public ListModule(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Provides
    @ListScope
    CityListAdapter provideCityListAdapter(CityListAdapter.AdapterClickListener clickListener) {
        return new CityListAdapter(clickListener);
    }

    @Provides
    @ListScope
    CityListAdapter.AdapterClickListener provideAdapterClickListener() {
        return mainActivity;
    }

    @Provides
    @ListScope
    LinearLayoutManager provideLinearLayoutManager(Context context) {
        return new LinearLayoutManager(context);
    }

    @Provides
    @ListScope
    CityListPresenter provideCityListPresenter(CityListPresenterImpl cityListPresenter) {
        return cityListPresenter;
    }
}

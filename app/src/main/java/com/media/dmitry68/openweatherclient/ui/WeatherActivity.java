package com.media.dmitry68.openweatherclient.ui;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.media.dmitry68.openweatherclient.R;
import com.media.dmitry68.openweatherclient.App;
import com.media.dmitry68.openweatherclient.base.BaseActivity;
import com.media.dmitry68.openweatherclient.di.ApplicationComponent;
import com.media.dmitry68.openweatherclient.domain.City;
import com.media.dmitry68.openweatherclient.presentation.WeatherPresenter;
import com.media.dmitry68.openweatherclient.utils.ImageUtil;
import com.media.dmitry68.openweatherclient.utils.NetworkUtil;

import java.util.Objects;

import javax.inject.Inject;

public class WeatherActivity extends BaseActivity implements WeatherView {
    private static final String ARG_CITY = "cityModel";

    @Inject
    Resources resources;

    @Inject
    WeatherPresenter weatherPresenter;

    @Inject
    Context context;

    private TextView cityTitleTv;
    private TextView tempTv;
    private ImageView cityIv;
    private ProgressBar progressBar;
    private City city;

    public static Intent newIntent(Context context, City city) {
        Intent intent = new Intent(context, WeatherActivity.class);
        intent.putExtra(ARG_CITY, city);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        if (getIntent() == null || getIntent().getExtras() == null || getIntent().getExtras().getParcelable(ARG_CITY) == null) {
            finish();
            return;
        }
        cityTitleTv = findViewById(R.id.city_title_tv);
        tempTv = findViewById(R.id.temp_tv);
        cityIv = findViewById(R.id.city_iv);
        progressBar = findViewById(R.id.progress_weather);

        city = getIntent().getExtras().getParcelable(ARG_CITY);
        if (city != null) {
            cityTitleTv.setText(city.getCityString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        weatherPresenter.bind(this);
        weatherPresenter.getCityWeatherDetails(city, NetworkUtil.isConnected(context));
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (weatherPresenter != null) {
            weatherPresenter.unbind();
        }
    }

    @Override
    protected void releaseSubComponents(App application) {
        App.releaseWeatherSubComponent();
    }

    @Override
    protected void injectDependencies(App application, ApplicationComponent component) {
        App.getWeatherSubComponent().inject(this);
    }

    @Override
    public void showQueryError(String message) {
        Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showPhoto(String photoUrl) {
        ImageUtil.loadAvatarFromUrl(context, photoUrl, cityIv);
    }

    @Override
    public void showTemperature(String temperature) {
        tempTv.setText(temperature);
    }


    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showOfflineMessage() {
        showQueryError(resources.getString(R.string.message_no_internet));
    }
}

package com.media.dmitry68.openweatherclient.presentation;

import com.media.dmitry68.openweatherclient.domain.City;
import com.media.dmitry68.openweatherclient.interactors.ManagerCity;
import com.media.dmitry68.openweatherclient.ui.ListView;

import java.util.Set;

import javax.inject.Inject;

public class CityListPresenterImpl implements CityListPresenter {
    private ListView cityListView;

    @Inject
    ManagerCity interactor;

    @Inject
    public CityListPresenterImpl() {

    }

    @Override
    public void getCities() {
        Set<City> cities = interactor.getCities();
        if (cityListView != null) {
            if (cities == null) {
                interactor.addCityToStorage("Москва");
                interactor.addCityToStorage("Санкт-Петербург");
                cityListView.setList(interactor.getCities());
                return;
            }
            cityListView.setList(cities);
        }
    }

    @Override
    public boolean isCityValid(String city) {
        return city != null && !city.isEmpty();
    }

    @Override
    public void addCity(String city) {
        if (interactor.addCityToStorage(city)) {
            Set<City> cities = interactor.getCities();
            cityListView.updateList(cities);
        }
    }

    @Override
    public void bind(ListView view) {
        cityListView = view;
    }

    @Override
    public void unbind() {
        cityListView = null;
    }
}

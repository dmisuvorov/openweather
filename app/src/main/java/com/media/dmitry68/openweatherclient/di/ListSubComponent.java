package com.media.dmitry68.openweatherclient.di;

import com.media.dmitry68.openweatherclient.di.modules.ListModule;
import com.media.dmitry68.openweatherclient.ui.MainActivity;

import dagger.Subcomponent;

@ListScope
@Subcomponent(modules = ListModule.class)
public interface ListSubComponent {
    void inject(MainActivity activity);
}

package com.media.dmitry68.openweatherclient.data;

import com.media.dmitry68.openweatherclient.domain.City;

import java.util.Set;

import javax.inject.Inject;

import io.reactivex.Single;

public class CityRepositoryImpl implements CityRepository {
    @Inject
    CityDataSource cityDataSource;

    @Inject
    StorageClient storageClient;

    @Inject
    public CityRepositoryImpl() {

    }

    @Override
    public Set<City> getCities() {
        return storageClient.getCitiesFromStorage();
    }

    @Override
    public boolean addCityToStorage(String cityName) {
        return addCityToStorage(cityDataSource.getCityByName(cityName));
    }

    @Override
    public boolean addCityToStorage(City city) {
        return storageClient.putCityToStorage(city);
    }

    @Override
    public Single<City> getDetailsCity(City city, boolean isConnected) {
        if (isConnected) {
            return cityDataSource.getCityDetailsByName(city);
        } else {
            return Single.fromCallable(() -> storageClient.getCityFromStorage(city));
        }
    }
}

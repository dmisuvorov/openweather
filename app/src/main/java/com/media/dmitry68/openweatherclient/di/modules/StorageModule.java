package com.media.dmitry68.openweatherclient.di.modules;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.reflect.TypeToken;
import com.media.dmitry68.openweatherclient.data.StorageClient;
import com.media.dmitry68.openweatherclient.domain.City;
import com.media.dmitry68.openweatherclient.framework.StorageClientImpl;
import com.media.dmitry68.openweatherclient.utils.Constants;

import java.lang.reflect.Type;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class StorageModule {

    @Provides
    @Singleton
    @Named("app_pref")
    String provideNameOfPref() {
        return Constants.APP_PREFERENCES;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(@Named("app_pref") String name, Context context) {
        return context.getSharedPreferences(name, Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    SharedPreferences.Editor provideEditorSharedPref(SharedPreferences sharedPreferences) {
        return sharedPreferences.edit();
    }

    @Provides
    @Singleton
    StorageClient provideStorageClient(StorageClientImpl storageClient) {
        return storageClient;
    }

    @Provides
    @Singleton
    @Named("setOfCities")
    Type provideTypeListOfCities() {
        return new TypeToken<TreeSet<City>>(){}.getType();
    }
}

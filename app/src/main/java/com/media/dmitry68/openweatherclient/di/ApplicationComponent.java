package com.media.dmitry68.openweatherclient.di;

import com.media.dmitry68.openweatherclient.di.modules.AndroidModule;
import com.media.dmitry68.openweatherclient.di.modules.ApplicationModule;
import com.media.dmitry68.openweatherclient.di.modules.CityModule;
import com.media.dmitry68.openweatherclient.di.modules.ClientModule;
import com.media.dmitry68.openweatherclient.di.modules.ListModule;
import com.media.dmitry68.openweatherclient.di.modules.NetworkModule;
import com.media.dmitry68.openweatherclient.di.modules.StorageModule;
import com.media.dmitry68.openweatherclient.di.modules.WeatherModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AndroidModule.class,
        ApplicationModule.class,
        CityModule.class,
        NetworkModule.class,
        ClientModule.class,
        StorageModule.class})
public interface ApplicationComponent {
    ListSubComponent plus(ListModule listModule);

    WeatherSubComponent plus(WeatherModule weatherModule);
}
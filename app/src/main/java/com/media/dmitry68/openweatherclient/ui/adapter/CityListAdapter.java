package com.media.dmitry68.openweatherclient.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.media.dmitry68.openweatherclient.R;
import com.media.dmitry68.openweatherclient.domain.City;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CityListAdapter extends RecyclerView.Adapter<CityListHolder> {
    private List<City> cities = new ArrayList<>();
    private AdapterClickListener clickListener;

    public CityListAdapter(AdapterClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public CityListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_city, viewGroup, false);
        return new CityListHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CityListHolder cityListHolder, final int i) {
        cityListHolder.bind(cities.get(i));
        cityListHolder.itemView.setOnClickListener(v -> OnItemClicked(i));
    }

    private void OnItemClicked(int position) {
        clickListener.onItemClicked(cities.get(position));
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    public void setList(Set<City> list) {
        cities = new ArrayList<>(list);
        notifyDataSetChanged();
    }

    public void updateList(Set<City> list) {
        List<City> newList = new ArrayList<>(list);
        DiffUtil.DiffResult diffResult = diffResult(newList);
        cities = newList;
        diffResult.dispatchUpdatesTo(this);
    }

    private DiffUtil.DiffResult diffResult(List<City> newList) {
        return DiffUtil.calculateDiff(new DiffUtil.Callback() {
            @Override
            public int getOldListSize() {
                return cities.size();
            }

            @Override
            public int getNewListSize() {
                return newList.size();
            }

            @Override
            public boolean areItemsTheSame(int oldPos, int newPos) {
                return cities.get(oldPos).equals(newList.get(newPos));
            }

            @Override
            public boolean areContentsTheSame(int oldPos, int newPos) {
                return cities.get(oldPos).hashCode() == newList.get(newPos).hashCode();
            }
        });
    }

    public interface AdapterClickListener {
        void onItemClicked(City city);
    }
}

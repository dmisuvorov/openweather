package com.media.dmitry68.openweatherclient.ui;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.media.dmitry68.openweatherclient.App;
import com.media.dmitry68.openweatherclient.R;
import com.media.dmitry68.openweatherclient.base.BaseActivity;
import com.media.dmitry68.openweatherclient.di.ApplicationComponent;
import com.media.dmitry68.openweatherclient.domain.City;
import com.media.dmitry68.openweatherclient.presentation.CityListPresenter;
import com.media.dmitry68.openweatherclient.ui.adapter.CityListAdapter;

import java.util.Set;

import javax.inject.Inject;

public class MainActivity extends BaseActivity implements ListView, CityListAdapter.AdapterClickListener {

    @Inject
    Context context;

    @Inject
    Resources resources;

    @Inject
    LinearLayoutManager layoutManager;

    @Inject
    CityListAdapter cityListAdapter;

    @Inject
    CityListPresenter cityListPresenter;

    private AutoCompleteTextView cityText;
    private RecyclerView repoList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cityText = findViewById(R.id.city_text);
        Button buttonAddCity = findViewById(R.id.button_add_city);
        repoList = findViewById(R.id.repo_list);
        buttonAddCity.setOnClickListener(v -> onButtonClick());
        initRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        cityListPresenter.bind(this);
        cityListPresenter.getCities();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (cityListPresenter != null) {
            cityListPresenter.unbind();
        }
    }

    private void initRecyclerView() {
        repoList.setAdapter(cityListAdapter);
        repoList.setLayoutManager(layoutManager);
    }

    private void onButtonClick() {
        cityText.setError(null);
        String cityName = cityText.getText().toString().trim();
        if (cityListPresenter.isCityValid(cityName)) {
            cityListPresenter.addCity(cityName);
        } else {
            cityText.setError(resources.getString(R.string.city_error));
            cityText.requestFocus();
        }
    }

    @Override
    protected void releaseSubComponents(App application) {
        App.releaseListSubComponent();
    }

    @Override
    protected void injectDependencies(App application, ApplicationComponent component) {
        App.getListSubComponent(this).inject(this);
    }

    @Override
    public void onItemClicked(City city) {
        startActivity(WeatherActivity.newIntent(context, city));
    }

    @Override
    public void setList(Set<City> cities) {
        cityListAdapter.setList(cities);
    }

    @Override
    public void updateList(Set<City> cities) {
        cityListAdapter.updateList(cities);
    }
}

package com.media.dmitry68.openweatherclient.di.modules;

import com.media.dmitry68.openweatherclient.utils.Constants;

import java.util.Random;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;

@Module
public class ApplicationModule {

    @Provides
    @Singleton
    @Named("weather")
    HttpUrl provideWeatherEndpoint() {
        return HttpUrl.parse(Constants.BASE_URL_WEATHER);
    }

    @Provides
    @Singleton
    @Named("photo")
    HttpUrl providePhotoEndpoint() {
        return HttpUrl.parse(Constants.BASE_URL_PHOTO);
    }

    @Provides
    @Singleton
    @Named("weather_api_key")
    String provideWeatherApiKey() {
        return Constants.BASE_WEATHER_API_KEY;
    }

    @Provides
    @Singleton
    @Named("photo_api_key")
    String providePhotoApiKey() {
        return Constants.BASE_PHOTO_API_KEY;
    }

    @Provides
    @Singleton
    Random provideRandom() {
        return new Random();
    }
}

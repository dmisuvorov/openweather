package com.media.dmitry68.openweatherclient.di.modules;

import com.media.dmitry68.openweatherclient.data.CityDataSource;
import com.media.dmitry68.openweatherclient.data.CityRepository;
import com.media.dmitry68.openweatherclient.data.CityRepositoryImpl;
import com.media.dmitry68.openweatherclient.framework.CityDataSourceImpl;
import com.media.dmitry68.openweatherclient.interactors.ManagerCity;
import com.media.dmitry68.openweatherclient.interactors.ManagerCityImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class CityModule {

    @Provides
    @Singleton
    ManagerCity provideInteractor(ManagerCityImpl managerCity) {
        return managerCity;
    }

    @Provides
    @Singleton
    CityRepository provideCityRepository(CityRepositoryImpl cityRepository) {
        return cityRepository;
    }

    @Provides
    @Singleton
    CityDataSource provideCityDataSource(CityDataSourceImpl cityDataSource) {
        return cityDataSource;
    }
}

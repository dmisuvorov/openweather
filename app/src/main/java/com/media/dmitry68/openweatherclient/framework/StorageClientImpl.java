package com.media.dmitry68.openweatherclient.framework;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.media.dmitry68.openweatherclient.data.StorageClient;
import com.media.dmitry68.openweatherclient.domain.City;
import com.media.dmitry68.openweatherclient.utils.Constants;

import java.lang.reflect.Type;
import java.util.Set;
import java.util.TreeSet;

import javax.inject.Inject;
import javax.inject.Named;

public class StorageClientImpl implements StorageClient {
    @Inject
    Gson gson;

    @Inject
    SharedPreferences sharedPreferences;

    @Inject
    SharedPreferences.Editor editor;

    @Inject
    @Named("setOfCities")
    Type typeOfListCities;

    @Inject
    public StorageClientImpl() {

    }

    @Override
    public boolean putCityToStorage(City city) {
        Set<City> cities = getCitiesFromStorage();
        if (cities == null) {
            cities = new TreeSet<>();
        }
        if (cities.contains(city)) {
            cities.remove(city);
            cities.add(city);
            putCitiesToStorage(cities);
            return true;
        }
        if (cities.add(city)) {
            putCitiesToStorage(cities);
            return true;
        }
        return false;
    }

    @Override
    public City getCityFromStorage(City city) {
        Set<City> cities = getCitiesFromStorage();
        for (City cityIterator : cities) {
            if (cityIterator.equals(city)) {
                return cityIterator;
            }
        }
        return null;
    }

    @Override
    public Set<City> getCitiesFromStorage() {
        String citiesJson = sharedPreferences.getString(Constants.KEY_CITIES, null);
        return gson.fromJson(citiesJson, typeOfListCities);
    }

    @Override
    public void putCitiesToStorage(Set<City> cities) {
        editor.putString(Constants.KEY_CITIES, gson.toJson(cities));
        editor.apply();
    }
}

package com.media.dmitry68.openweatherclient.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.media.dmitry68.openweatherclient.R;
import com.media.dmitry68.openweatherclient.domain.City;

public class CityListHolder extends RecyclerView.ViewHolder {
    private TextView cityTv;


    public CityListHolder(@NonNull View itemView) {
        super(itemView);
        cityTv = itemView.findViewById(R.id.city_item_tv);
    }

    void bind(City city) {
        cityTv.setText(city.getCityString());
    }
}

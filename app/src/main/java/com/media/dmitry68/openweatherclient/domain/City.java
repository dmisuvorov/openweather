package com.media.dmitry68.openweatherclient.domain;

import android.os.Parcel;
import android.os.Parcelable;

public final class City implements Parcelable, Comparable<City> {
    private final String cityString;
    private String cityPhotoUrl;
    private Double temperature;

    private City(String cityString, String cityPhotoUrl, Double temperature) {
        this.cityString = cityString;
        this.cityPhotoUrl = cityPhotoUrl;
        this.temperature = temperature;
    }

    protected City(Parcel in) {
        cityString = in.readString();
        cityPhotoUrl = in.readString();
        if (in.readByte() == 0) {
            temperature = null;
        } else {
            temperature = in.readDouble();
        }
    }

    public static final Creator<City> CREATOR = new Creator<City>() {
        @Override
        public City createFromParcel(Parcel in) {
            return new City(in);
        }

        @Override
        public City[] newArray(int size) {
            return new City[size];
        }
    };

    public String getCityString() {
        return cityString;
    }

    public String getCityPhotoUrl() {
        return cityPhotoUrl;
    }

    public Double getTemperature() {
        return temperature;
    }

    public static Builder newBuilder() {
        return new City.Builder();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cityString);
        dest.writeString(cityPhotoUrl);
        if (temperature == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(temperature);
        }
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + cityString.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
       if (obj == this) {
           return true;
       }
       if (!(obj instanceof City)) {
           return false;
       }
       City city = (City) obj;
       return city.cityString.equalsIgnoreCase(this.cityString);
    }

    @Override
    public int compareTo(City o) {
        return this.cityString.compareToIgnoreCase(o.cityString);
    }


    public static class Builder {
        private String cityString = "";
        private String cityPhotoUrl = "";
        private Double temperature = 0.0d;

        private Builder() {

        }

        public Builder setCityString(String city) {
            this.cityString = city.trim();
            return this;
        }

        public Builder setCityPhotoUrl(String cityPhotoUrl) {
            this.cityPhotoUrl = cityPhotoUrl;
            return this;
        }

        public Builder setTemperature(Double temperature) {
            this.temperature = temperature;
            return this;
        }

        public City build() {
            return new City(cityString, cityPhotoUrl, temperature);
        }
    }
}

package com.media.dmitry68.openweatherclient.di.modules;

import com.media.dmitry68.openweatherclient.di.WeatherScope;
import com.media.dmitry68.openweatherclient.presentation.WeatherPresenter;
import com.media.dmitry68.openweatherclient.presentation.WeatherPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class WeatherModule {

    @Provides
    @WeatherScope
    WeatherPresenter provideWeatherPresenter(WeatherPresenterImpl weatherPresenter) {
        return weatherPresenter;
    }
}

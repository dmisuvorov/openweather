package com.media.dmitry68.openweatherclient.utils;

public class Constants {
    public static final String APP_PREFERENCES = "pref";

    public static final String BASE_URL_WEATHER = "https://api.openweathermap.org/data/2.5/";
    public static final String BASE_URL_PHOTO = "https://pixabay.com/api/";

    public static final String BASE_WEATHER_API_KEY = "2321058ae44fb1a886660ad6c1f1ae08";
    public static final String BASE_PHOTO_API_KEY = "13898996-e322a6b56dadd989a6eb0394e";

    public static final String KEY_CITIES = "cities";
}

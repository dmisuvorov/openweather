package com.media.dmitry68.openweatherclient.data;

import com.media.dmitry68.openweatherclient.domain.City;

import java.util.Set;

import io.reactivex.Single;

public interface CityRepository {
    Set<City> getCities();

    boolean addCityToStorage(String cityName);

    boolean addCityToStorage(City city);

    Single<City> getDetailsCity(City city, boolean isConnected);
}

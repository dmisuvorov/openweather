package com.media.dmitry68.openweatherclient.data;

import com.media.dmitry68.openweatherclient.domain.City;

import io.reactivex.Single;

public interface CityDataSource {

    City getCityByName(String name);

    Single<City> getCityDetailsByName(String name);

    Single<City> getCityDetailsByName(City city);

    Single<Double> getWeatherByName(String name);

    Single<String> getPhotoUrlByName(String name);
}
